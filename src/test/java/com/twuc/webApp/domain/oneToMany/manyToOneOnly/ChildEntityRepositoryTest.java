package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValueForChildEntity = new ClosureValue<>();

        flush(em -> {
            parentEntityRepository.save(parentEntity);
            childEntity.setParentEntity(parentEntity);
            ChildEntity savedChildEntity = childEntityRepository.save(childEntity);
            closureValueForChildEntity.setValue(savedChildEntity.getId());
        });

        run(em -> {
            Optional<ChildEntity> childEntityOptional = childEntityRepository.findById(closureValueForChildEntity.getValue());
            assertNotNull(childEntityOptional.get().getParentEntity());
        });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValueForChildEntity = new ClosureValue<>();
        ClosureValue<Long> closureValueForParentEntity = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
            childEntity.setParentEntity(parentEntity);
            ChildEntity savedChildEntity = childEntityRepository.save(childEntity);
            closureValueForChildEntity.setValue(savedChildEntity.getId());
            closureValueForParentEntity.setValue(savedParentEntity.getId());
        });

        flushAndClear(em -> {
            childEntity.setParentEntity(null);
            childEntityRepository.save(childEntity);
        });

        run(em -> {
            Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(closureValueForParentEntity.getValue());
            assertNotNull(parentEntityOptional.get());
            Optional<ChildEntity> childEntityOptional = childEntityRepository.findById(closureValueForChildEntity.getValue());
            assertNotNull(childEntityOptional.get());
            assertNull(childEntityOptional.get().getParentEntity());
        });

        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");
        ClosureValue<Long> closureValueForChildEntity = new ClosureValue<>();
        ClosureValue<Long> closureValueForParentEntity = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity savedParentEntity = parentEntityRepository.save(parentEntity);
            childEntity.setParentEntity(parentEntity);
            ChildEntity savedChildEntity = childEntityRepository.save(childEntity);
            closureValueForChildEntity.setValue(savedChildEntity.getId());
            closureValueForParentEntity.setValue(savedParentEntity.getId());
        });

        flushAndClear(em -> {
            childEntityRepository.deleteById(closureValueForChildEntity.getValue());
            parentEntityRepository.deleteById(closureValueForParentEntity.getValue());
        });

        run(em -> {
            Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(closureValueForParentEntity.getValue());
            assertFalse(parentEntityOptional.isPresent());
            Optional<ChildEntity> childEntityOptional = childEntityRepository.findById(closureValueForChildEntity.getValue());
            assertFalse(childEntityOptional.isPresent());
        });
        // --end-->
    }
}
